import useInpuState from './hooks/useInputState';
import { Container } from 'react-bootstrap';

export default function TransactionForm(props) {
	console.log('PROPS', props);
	const [name, setName, resetName] = useInpuState('No Category');
	const [type, setType, resetType] = useInpuState('income');
	const [amount, setAmount, resetAmount] = useInpuState('');
	const [description, setDescription, resetDescription] = useInpuState('');

	function handleSubmit(e) {
		e.preventDefault();
		const userData = {
			name,
			type,
			amount,
			description,
		};

		props.onAddTransaction(userData);
		resetAmount();
		resetDescription();
	}

	let options = '';

	if (props.userData) {
		const filteredCategory = props.userData.categories.filter(
			(category) => category.type.toLowerCase() === type.toLowerCase()
		);

		options = filteredCategory.map((category) => {
			return (
				<option key={category._id} value={category.name}>
					{category.name}
				</option>
			);
		});
	}
	console.log(type);
	console.log(name);

	return (
		<form className='card' onSubmit={handleSubmit} autoComplete='off'>
			<h1>Transaction</h1>
			<h2>
				Add you income and expenses below or <span>Add Category</span>
			</h2>

			<div className='field'>
				<select id='type' onChange={setType}>
					<option value='income'>Income</option>
					<option value='expenses'>Expenses</option>
				</select>
			</div>

			<div className='field'>
				<select id='name' onClick={setName}>
					<option value='No Category'>No Category</option>
					{options}
					{/* <option value='category-1'>Category-1</option>
						<option value='category-2'>Category-2</option> */}
				</select>
			</div>

			<div className='field'>
				<input
					className='input'
					type='text'
					name='amount'
					id='amount'
					value={amount}
					onChange={setAmount}
					required
				/>
				<label className='label' htmlFor='amount'>
					Amount
				</label>
			</div>

			<div className='field'>
				<input
					className='input'
					type='text'
					name='description'
					id='description'
					value={description}
					onChange={setDescription}
					required
				/>
				<label className='label' htmlFor='description'>
					Description
				</label>
			</div>

			<button type='submit'>Add</button>
		</form>
	);
}

{
	/* <Form style={{ width: '30rem' }} onSubmit={handleSubmit}>
<Form.Group>
	<Form.Label>Category Type</Form.Label>
	<Form.Control as='select'>
		<option>Income</option>
		<option>Expenses</option>
	</Form.Control>
</Form.Group>
<Form.Group>
	<Form.Label>Category Name</Form.Label>
	<Form.Control as='select'>
		<option>Category 1</option>
		<option>Category 2</option>
	</Form.Control>
</Form.Group>
<Form.Group>
	<Form.Label>Amount</Form.Label>
	<Form.Control
		type='text'
		placeholder='Amount'
		value={amount}
		onChange={setAmount}
	/>
</Form.Group>
<Form.Group>
	<Form.Label>Description</Form.Label>
	<Form.Control
		as='textarea'
		placeholder='Description'
		value={description}
		onChange={setDescription}
	/>
</Form.Group>

<Button variant='primary' type='submit'>
	Register
</Button>
</Form> */
}
