import React, { useState, useEffect } from 'react';
import useInpuState from './hooks/useInputState';
import { Container } from 'react-bootstrap';

export default function RegisterForm(props) {
	const [firstName, setfirstName, resetfirstName] = useInpuState('');
	const [lastName, setlastName, resetlastName] = useInpuState('');
	const [mobileNo, setMobileNo, resetMobileNo] = useInpuState('');
	const [email, setEmail, resetEmail] = useInpuState('');
	const [password, setPassword, resetPassword] = useInpuState('');
	const [verifyPassword, setVerifyPassword, resetVerifyPassword] = useInpuState(
		''
	);

	function handleSubmit(e) {
		e.preventDefault();
		const userData = {
			firstName,
			lastName,
			mobileNo,
			email,
			password,
		};

		props.onRegister(userData);
	}

	return (
		<Container>
			<form className='card' onSubmit={handleSubmit} autoComplete='off'>
				<h1>Create account</h1>
				<h2>
					Already have an account? <span>Sign in</span>
				</h2>

				<div className='field'>
					<input
						className='input'
						type='text'
						name='firstName'
						id='firstName'
						value={firstName}
						onChange={setfirstName}
						required
					/>
					<label className='label' htmlFor='firstName'>
						First Name
					</label>
				</div>

				<div className='field'>
					<input
						className='input'
						type='text'
						name='lastName'
						id='lastName'
						value={lastName}
						onChange={setlastName}
						required
					/>
					<label className='label' htmlFor='lastName'>
						Last Name
					</label>
				</div>

				<div className='field'>
					<input
						className='input'
						type='text'
						name='mobileNo'
						id='mobileNo'
						value={mobileNo}
						onChange={setMobileNo}
						required
					/>
					<label className='label' htmlFor='mobileNo'>
						Mobile No
					</label>
				</div>

				<div className='field'>
					<input
						className='input'
						type='email'
						name='email'
						id='email'
						value={email}
						onChange={setEmail}
						required
					/>
					<label className='label' htmlFor='email'>
						Email
					</label>
				</div>

				<div className='field'>
					<input
						className='input'
						type='password'
						name='password'
						id='password'
						value={password}
						onChange={setPassword}
						required
					/>
					<label className='label' htmlFor='password'>
						Password
					</label>
				</div>

				<div className='field'>
					<input
						className='input'
						type='password'
						name='verifyPassword'
						id='verifyPassword'
						value={verifyPassword}
						onChange={setVerifyPassword}
						required
					/>
					<label className='label' htmlFor='verifyPassword'>
						Verify Password
					</label>
				</div>

				<button type='submit'>Sign up</button>
			</form>
		</Container>
	);
}

{
	/* <Form style={{ width: '30rem' }} onSubmit={handleSubmit}>
<Form.Group>
	<Form.Label>First Name</Form.Label>
	<Form.Control
		type='text'
		placeholder='First Name'
		value={firstName}
		onChange={setfirstName}
	/>
</Form.Group>
<Form.Group>
	<Form.Label>Last Name</Form.Label>
	<Form.Control
		type='text'
		placeholder='Last Name'
		value={lastName}
		onChange={setlastName}
	/>
</Form.Group>
<Form.Group>
	<Form.Label>Mobile No</Form.Label>
	<Form.Control
		type='text'
		placeholder='Mobile'
		value={mobileNo}
		onChange={setMobileNo}
	/>
</Form.Group>
<Form.Group>
	<Form.Label>Email address</Form.Label>
	<Form.Control
		type='email'
		placeholder='Email'
		value={email}
		onChange={setEmail}
	/>
</Form.Group>

<Form.Group>
	<Form.Label>Password</Form.Label>
	<Form.Control
		type='password'
		placeholder='Password'
		value={password}
		onChange={setPassword}
	/>
</Form.Group>

<Form.Group>
	<Form.Label>Verify Password</Form.Label>
	<Form.Control
		type='password'
		placeholder='Verify Password'
		value={verifyPassword}
		onChange={setVerifyPassword}
	/>
</Form.Group>

<Button variant='primary' type='submit'>
	Register
</Button>
</Form> */
}
