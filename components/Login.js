import React from 'react';
import { GoogleLogin } from 'react-google-login';
import { refreshTokenSetup } from './helpers/helpers';

const clientId =
	'41810439145-o9q5bu3okt9urh78m55c1rfanhet22j5.apps.googleusercontent.com';

function Login() {
	const onSuccess = (res) => {
		console.log('Login Success: currentUser:', res.profileObj);
		alert(
			`Logged in successfully welcome ${res.profileObj.name} 😍. \n See console for full profile object.`
		);
		refreshTokenSetup(res);
	};

	const onFailure = (res) => {
		console.log('Login failed: res:', res);
		alert(
			`Failed to login. 😢 Please ping this to repo owner twitter.com/sivanesh_fiz`
		);
	};

	return (
		<div style={{ margin: '20px  auto 0 auto' }}>
			<GoogleLogin
				clientId={clientId}
				onSuccess={onSuccess}
				onFailure={onFailure}
				cookiePolicy={'single_host_origin'}
				style={{ marginTop: '100px' }}
				isSignedIn={true}
			/>
		</div>
	);
}

export default Login;
