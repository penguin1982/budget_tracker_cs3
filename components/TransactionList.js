import { Container, Table } from 'react-bootstrap';
import moment from 'moment';

export default function TransactionList(props) {
	let list = '';

	if (props.transactions) {
		list = props.transactions.map((transaction) => {
			return (
				<tr>
					<td>{transaction.amount}</td>
					<td>{transaction.runningBalance}</td>
					<td>{transaction.type}</td>
					<td>{moment(transaction.dateAdded).format('llll')}</td>
					<td>{transaction.description}</td>
					<td>{transaction.name}</td>
				</tr>
			);
		});
	}

	return (
		<Table striped bordered hover className='mt-5'>
			<thead>
				<tr>
					<th>Amount</th>
					<th>Running Balance</th>
					<th>Type</th>
					<th>Date Added</th>
					<th>Description</th>
					<th>Name</th>
				</tr>
			</thead>
			<tbody>{list}</tbody>
		</Table>
	);
}

// {
// 	<div className='card'>
// <h1>Transaction Summary</h1>
// <ul>{list}</ul>
// </div>
// }
