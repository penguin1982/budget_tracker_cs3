import { Navbar, Nav } from 'react-bootstrap';
import Link from 'next/link';
import { useEffect, useState } from 'react';

export default function NavBar() {
	const [isLogin, setIsLogin] = useState(false);

	useEffect(() => {
		const token = window.localStorage.getItem('token');
		if (token) {
			setIsLogin(true);
		} else {
			setIsLogin(false);
		}
	});

	return (
		<Navbar bg='success' expand='lg' fixed='top' variant='dark'>
			<Link href='/'>
				<a className='navbar-brand'>Budgetarian</a>
			</Link>
			<Navbar.Toggle aria-controls='basic-navbar-nav' />
			<Navbar.Collapse className='basic-navbar-nav'>
				<Nav className='mr-auto'>
					{!isLogin ? (
						<>
							<Link href='/'>
								<a className='nav-link'>Login</a>
							</Link>
							<Link href='/register'>
								<a className='nav-link'>Register</a>
							</Link>
						</>
					) : (
						<>
							<Link href='/logout'>
								<a className='nav-link'>Logout</a>
							</Link>
							<Link href='/add-category'>
								<a className='nav-link'>Add Category</a>
							</Link>
							<Link href='/add-transaction'>
								<a className='nav-link'>Add Transaction</a>
							</Link>
							<Link href='/reports'>
								<a className='nav-link'>Reports</a>
							</Link>
						</>
					)}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
}
