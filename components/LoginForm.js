import React from 'react';
import useInpuState from './hooks/useInputState';
import Login from './Login';
import { Form, Button, Container } from 'react-bootstrap';

export default function LoginForm(props) {
	const [email, setEmail, resetEmail] = useInpuState('');
	const [password, setPassword, resetPassword] = useInpuState('');

	function handleSubmit(e) {
		e.preventDefault();
		const userData = {
			email,
			password,
		};

		props.onLogin(userData);
	}

	return (
		<Container>
			<form className='card' onSubmit={handleSubmit} autoComplete='off'>
				<h1>Login</h1>
				<h2>
					Don't have an account yet? <span>Register</span>
				</h2>

				<div className='field'>
					<input
						className='input'
						type='email'
						name='email'
						id='email'
						value={email}
						onChange={setEmail}
						required
					/>
					<label className='label' htmlFor='email'>
						Email
					</label>
				</div>

				<div className='field'>
					<input
						className='input'
						type='password'
						name='password'
						id='password'
						value={password}
						onChange={setPassword}
						required
					/>
					<label className='label' htmlFor='password'>
						Password
					</label>
				</div>

				<button type='submit'>Sign In</button>
				{/* <Login></Login> */}
			</form>
		</Container>
	);
}

{
	/* <Form style={{ width: '30rem' }} onSubmit={handleSubmit}>
<Form.Group>
	<Form.Label>Email address</Form.Label>
	<Form.Control
		type='email'
		placeholder='Email'
		value={email}
		onChange={setEmail}
	/>
</Form.Group>

<Form.Group>
	<Form.Label>Password</Form.Label>
	<Form.Control
		type='password'
		placeholder='Password'
		value={password}
		onChange={setPassword}
	/>
</Form.Group>
<Button variant='primary' type='submit'>
	Login
</Button>
</Form> */
}
