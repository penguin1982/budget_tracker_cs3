import React from 'react';
import useInpuState from './hooks/useInputState';

import { Container } from 'react-bootstrap';

export default function CategoryForm(props) {
	const [name, setName, resetName] = useInpuState('');
	const [type, setType, resetType] = useInpuState('income');

	function handleSubmit(e) {
		e.preventDefault();
		const categoryData = {
			name,
			type,
		};

		props.onAddCategory(categoryData);
		resetName();
	}

	return (
		<Container>
			<form className='card' onSubmit={handleSubmit} autoComplete='off'>
				<h1>Category</h1>
				<h2>
					Create your own categories <span>Add Transaction</span>
				</h2>
				<div className='field'>
					<input
						className='input'
						type='text'
						name='name'
						id='name'
						value={name}
						onChange={setName}
						required
					/>
					<label className='label' htmlFor='name'>
						Name
					</label>
				</div>

				<div className='field'>
					<select id='type' onChange={setType}>
						<option value='income'>Income</option>
						<option value='expenses'>Expenses</option>
					</select>
				</div>

				<button type='submit'>Add</button>
			</form>
		</Container>
	);
}

{
	/* <Form style={{ width: '30rem' }} onSubmit={handleSubmit}>
<Form.Group>
	<Form.Label>Category Name</Form.Label>
	<Form.Control
		type='text'
		placeholder='Category Name'
		value={name}
		onChange={setName}
	/>
</Form.Group>
<Form.Group>
	<Form.Label>Category Type</Form.Label>
	<Form.Control as='select' value={type} onChange={setType}>
		<option selected value='income'>
			Income
		</option>
		<option value='expenses'>Expenses</option>
	</Form.Control>
</Form.Group>

<Button variant='primary' type='submit'>
	Add
</Button>
</Form> */
}
