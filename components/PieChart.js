import { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';
import { colorRandomizer } from '../components/helpers/helpers';

export default function PieChart({ userData }) {
	console.log(userData);
	const [income, setIncome] = useState([]);
	const [expenses, setExpenses] = useState([]);
	const [bgColors, setBgColors] = useState([]);

	useEffect(() => {
		if (userData) {
			const getSum = (total, num) => {
				return total + Math.abs(num);
			};
			const incomeData = userData
				.filter((element) => {
					return element.type.toLowerCase() === 'income';
				})
				.map((element) => element.amount);

			console.log('INCOME DATA', incomeData);

			const expensesData = userData
				.filter((element) => {
					return element.type.toLowerCase() === 'expenses';
				})
				.map((element) => element.amount);

			console.log('EXPENSES DATA', expensesData);

			setIncome(incomeData.reduce(getSum, 0));
			setExpenses(expensesData.reduce(getSum, 0));

			setBgColors(userData.map(() => `#${colorRandomizer()}`));
		}
	}, [userData]);

	const data = {
		labels: ['Income', 'Expenses'],
		datasets: [
			{
				data: [income, expenses],
				backgroundColor: bgColors,
				hoverBackgroundColor: bgColors,
			},
		],
	};
	return <Pie data={data} />;
}

{
	/* <Pie data={data} />; */
}
