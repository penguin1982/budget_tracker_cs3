import { createContext, useState } from 'react';

const UserContext = createContext({
	userData: '',
	addUserData: (data) => {},
});

export function UserContextProvider(props) {
	const [userData, setUserData] = useState('');

	const context = {
		userData: userData,
		addUserData: addUserDataHandler,
	};
	function addUserDataHandler(data) {
		setUserData(data);
	}
	return (
		<UserContext.Provider value={context}>
			{props.children}
		</UserContext.Provider>
	);
}

export default UserContext;
