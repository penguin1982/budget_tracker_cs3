import RegisterForm from '../../components/RegisterForm';
import View from '../../components/View';
import Router from 'next/router';

import { toast } from 'react-toastify';

export default function RegisterPage() {
	async function handleRegister(data) {
		const res = await fetch(
			'https://agile-dusk-71560.herokuapp.com/api/users/email-exists',
			{
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({ email: data.email }),
			}
		);

		const isEmailExist = await res.json();

		if (isEmailExist) {
			toast.info('The email is taken. Try another', {
				position: 'top-center',
			});
		} else {
			const response = await fetch(
				'https://agile-dusk-71560.herokuapp.com/api/users/register',
				{
					method: 'POST',
					body: JSON.stringify(data),
					headers: {
						'Content-Type': 'application/json',
					},
				}
			);
			const responseData = await response.json();

			Router.push('/');
			toast.success(
				'Congratulations, your account has been successfully created',
				{
					position: 'top-center',
				}
			);
		}
	}
	return (
		<View title='Register'>
			<RegisterForm onRegister={handleRegister} />
		</View>
	);
}

// const res = await fetch('http://localhost:4000/api/users/email-exists', {
// const response = await fetch('http://localhost:4000/api/users/register', {
