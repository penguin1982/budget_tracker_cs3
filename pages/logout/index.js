import Router from 'next/router';
import { useEffect } from 'react';

export default function LogoutPage() {
	useEffect(() => {
		localStorage.clear();
		Router.push('/');
	});

	return <h1>Logging out...</h1>;
}
