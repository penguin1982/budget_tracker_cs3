import { toast } from 'react-toastify';
import CategoryForm from '../../components/CategoryForm';
import View from '../../components/View';

export default function AddCategoryPage() {
	async function handleAddCategory(data) {
		const token = localStorage.getItem('token');
		// const response = await fetch(
		// 	'http://localhost:4000/api/users/add-category',
		// 	{
		const response = await fetch(
			'https://agile-dusk-71560.herokuapp.com/api/users/add-category',
			{
				method: 'POST',
				body: JSON.stringify(data),
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`,
				},
			}
		);
		const responseData = await response.json();
		console.log(responseData);
		if (responseData === true) {
			toast.success('Added successfully', {
				position: 'top-center',
			});
		} else {
			toast.info('Something went wrong', {
				position: 'top-center',
			});
		}
	}
	return (
		<View title='Add Category'>
			<CategoryForm onAddCategory={handleAddCategory} />
		</View>
	);
}
