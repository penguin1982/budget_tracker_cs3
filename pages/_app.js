import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.min.css';

import '../styles/globals.css';
import styles from '../styles/Container.module.css';

import { Container } from 'react-bootstrap';

import { UserContextProvider } from '../store/user-context';
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';
import { ToastContainer } from 'react-toastify';

function MyApp({ Component, pageProps }) {
	return (
		<UserContextProvider>
			<ToastContainer />
			<NavBar />
			<Container fluid>
				<Component {...pageProps} />
			</Container>
		</UserContextProvider>
	);
}

export default MyApp;
