import { useState, useContext } from 'react';
import Router from 'next/router';
import { toast } from 'react-toastify';

import LoginForm from '../components/LoginForm';
import View from '../components/View';

import UserContext from '../store/user-context';

export default function Home() {
	// const [userData, setUserData] = useState('');

	const userContext = useContext(UserContext);

	async function handleLogin(data) {
		const response = await fetch('http://localhost:4000/api/users/login', {
			// const response = await fetch(
			// 	'https://agile-dusk-71560.herokuapp.com/api/users/login',
			// {
			method: 'POST',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		});

		const responseData = await response.json();

		if (responseData.access) {
			window.localStorage.setItem('token', responseData.access);
			const response = await fetch('http://localhost:4000/api/users/details', {
				// const response = await fetch(
				// 	'https://agile-dusk-71560.herokuapp.com/api/users/details',
				// 	{
				headers: {
					Authorization: `Bearer ${responseData.access}`,
				},
			});

			const dataResponse = await response.json();

			userContext.addUserData(dataResponse);

			Router.push('/add-transaction');
			toast.success('Welcome Back', {
				position: 'top-center',
			});
		} else {
			toast.info('Incorrect username or password', {
				position: 'top-center',
			});
		}
	}
	return (
		<View title='Login'>
			<LoginForm onLogin={handleLogin} />
		</View>
	);
}
