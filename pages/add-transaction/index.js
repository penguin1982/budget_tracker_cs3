import { useContext, useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { Row, Col } from 'react-bootstrap';

import UserContext from '../../store/user-context';
import TransactionForm from '../../components/TransactionForm';
import TransactionList from '../../components/TransactionList';

import View from '../../components/View';

export default function TransactionPage() {
	const userContext = useContext(UserContext);
	const [status, setStatus] = useState('idle');
	const [user, setUser] = useState();
	const [category, setCategory] = useState();
	const [transaction, setTransaction] = useState();

	useEffect(() => {
		const fetchData = async () => {
			const token = window.localStorage.getItem('token');
			setStatus('fetching');
			const response = await fetch(
				'https://agile-dusk-71560.herokuapp.com/api/users/details',
				{
					// const response = await fetch('http://localhost:4000/api/users/details', {
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${token}`,
					},
				}
			);
			const dataResponse = await response.json();

			setUser(dataResponse);

			if (dataResponse) {
				setStatus('fetched');
				setTransaction(dataResponse.transactions);
			}
		};

		fetchData();
	}, [category]);

	async function handleAddTransaction(data) {
		const token = localStorage.getItem('token');
		const response = await fetch(
			// 'http://localhost:4000/api/users/add-transaction',
			'https://agile-dusk-71560.herokuapp.com/api/users/add-transaction',
			{
				method: 'POST',
				body: JSON.stringify(data),
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`,
				},
			}
		);
		const responseData = await response.json();

		if (responseData !== false) {
			console.log('inside if', responseData);
			setTransaction(responseData);
			toast.success('Added successfully', {
				position: 'top-center',
			});
		} else {
			toast.info('Something went wrong', {
				position: 'top-center',
			});
		}
	}

	return (
		<View title='Add Transaction'>
			<Row>
				<Col>
					{status === 'fetching' ? (
						'Loading....'
					) : (
						<TransactionForm
							userData={user}
							onAddTransaction={handleAddTransaction}
						/>
					)}
				</Col>
			</Row>
			<Row>
				<Col>
					{status === 'fetching' ? (
						'Loading....'
					) : (
						<TransactionList
							userTransactions={user}
							transactions={transaction}
						></TransactionList>
					)}
				</Col>
			</Row>
		</View>
	);
}

// {status === 'fetching' ? 'Loading....' : 'Fetch'}

{
	/* <TransactionForm
					// userData={userContext.userData}
					onAddTransaction={handleAddTransaction}
				/> */
}
