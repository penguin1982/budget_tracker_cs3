import { useContext, useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { Row, Col, Container } from 'react-bootstrap';

import View from '../../components/View';
import PieChart from '../../components/PieChart';

export default function ReportPage() {
	const [status, setStatus] = useState('idle');
	const [user, setUser] = useState();
	const [category, setCategory] = useState();
	const [transaction, setTransaction] = useState();

	useEffect(() => {
		const fetchData = async () => {
			const token = window.localStorage.getItem('token');
			setStatus('fetching');
			const response = await fetch(
				'https://agile-dusk-71560.herokuapp.com/api/users/details',
				{
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${token}`,
					},
				}
			);
			const dataResponse = await response.json();

			setUser(dataResponse);

			if (dataResponse) {
				setStatus('fetched');
				setTransaction(dataResponse.transactions);
			}
		};

		fetchData();
	}, [category]);

	return (
		<View title='Add Transaction'>
			{status === 'fetching' ? (
				'Loading....'
			) : (
				<Container fluid>
					<Row>
						<Col>
							<h1>Income/Expenses</h1>
							<PieChart userData={transaction} />
						</Col>
					</Row>
				</Container>
			)}
		</View>
	);
}
